const express = require("express");
const bodyParser = require("body-parser");
const fs = require("fs");
const app = express();
const PORT = 3000;
const cors = require("cors");
app.use(cors());

const databasePath = "./data.json";

app.use(bodyParser.json());
app.listen(PORT, () => {
  console.log("server running on localhost:" + PORT);
});

if (
  !fs.existsSync(databasePath) ||
  fs.readFileSync(databasePath, "utf8").length === 0
) {
  fs.writeFileSync(databasePath, "[]", { flag: "w" });
}

app.get("/", (req, res) => {
  res.send("get data on localhost:" + PORT + "/api");
});

app.get("/api", (req, res) => {
  try {
    const data = fs.readFileSync(databasePath, "utf8");
    res.status(200).json(JSON.parse(data));
  } catch (error) {
    console.error("Error get history: " + error);
    res.status(500).send("Error get game history");
  }
});

app.post("/api", async (req, res) => {
  try {
    const body = req.body;
    const currentDate = new Date().toISOString();
    const gameData = {
      date: currentDate,
      gameHistory: body.gameHistory,
      pattern: body.pattern,
    };

    const data = fs.readFileSync(databasePath, "utf8");
    const gameHistory = JSON.parse(data);
    gameHistory.push(gameData);

    fs.writeFileSync(databasePath, JSON.stringify(gameHistory, null, 2));
    res.status(200).send("Game History Received and Saved");
  } catch (error) {
    console.error("Error posting game history: " + error);
    res.status(500).send("Error saving game history");
  }
});

app.delete("/api", async (req, res) => {
  try {
    const data = fs.readFileSync(databasePath, "utf8");
    const gameHistory = JSON.parse(data);
    gameHistory.splice(0, 1);
    fs.writeFileSync(databasePath, JSON.stringify(gameHistory, null, 2));
    res.status(200).send("Game History Deleted");
  } catch (error) {
    console.error("Error deleting game history: " + error);
    res.status(500).send("Error deleting game history");
  }
});
