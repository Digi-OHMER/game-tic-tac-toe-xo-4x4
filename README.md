# GAME Tic Tac Toe - XO 4x4

**LAB**

- ✅ เกม XO สามารถกำหนด ขนาดตารางของ XO นอกจาก 3x3 เป็นขนาดใด ๆ ก็ได้
- ✅ มีระบบฐานข้อมูลเก็บ history play เพื่อดู replay ได้
- ✅ bonus point มี AI ระบบ bot คู่ต่อสู้ ที่เล่นกับมนุษย์อัตโนมัติได้ [โจทย์นี้เป็น optional ทำหรือไม่ทำก็ได้] // update แล้ว มี AI ระบบ bot คู่ต่อสู้

**Project setup**

- git clone https://gitlab.com/Digi-OHMER/game-tic-tac-toe-xo-4x4
- cd game-tic-tac-toe-xo-4x4/
- terminal 1 : cd BackEnd/
- terminal 1 : node server.js
- terminal 2 : cd FrontEnd/
- terminal 2 : yarn add vite
- terminal 2 : yarn dev

**Enjoy GAME Tic Tac Toe - XO 4x4**

ในวิดีโอไม่ได้มีการกำหนดขนาดแต่ได้ทำการอัพเดทไว้ในตัวโค้ดแล้วครับ

Link video setup: **[https://youtu.be/MIE_Hpk8ISI](https://youtu.be/MIE_Hpk8ISI)**

[![Watch the video](https://img.youtube.com/vi/MIE_Hpk8ISI/maxresdefault.jpg)](https://youtu.be/MIE_Hpk8ISI)

# **วิธีออกแบบโปรแกรมและ Algorithm**

**FrontEnd : Display Process** **: Tailwind, Ant Design**

- มีการกำหนดขนาด ด้าน\*ด้าน ที่ ขนาด 1 ด้าน มีค่า >= 3
- ทำการ Loop Rows ตามจำนวนด้าน และ Columns อยู่ข้างใน Rows อีกที
- มีการแสดงปุ่มแสดงประวัติการเล่น ไป, กลับ เมื่อทำการดูประวัติการเล่น ระบบจะทำการเก็บสถานะว่า จะดูประวัติหรือทำการเล่น โดยเก็บเป็น Boolean เพื่อกันว่าเราจะทำการเล่นระหว่างดูประวิติ
- แสดงผลเมือมีผู้ชนะหรือไม่สามารถหาผู้ชนะได้

**FrontEnd : Logical Process** **: React, Typescript**

- กำหนดขนาดที่ต้องการเล่น ด้าน\*ด้าน ที่ ขนาด 1 ด้าน มีค่า >= 3
- นำขนาดไปคำนวณหา patterns ที่สามารถชนะได้โดยเก็บเป็น Array ดังตัวอย่าง

![Alt Text](https://cdn.discordapp.com/attachments/1177081956429733948/1251038019637743666/image.png?ex=666d1f73&is=666bcdf3&hm=3e738dd3d690140891b556536198bbc6451bbca32f43ac00d0d2b333a179f289&)

- เก็บประวัติการเล่นตามปุ่มลำดับการคลิก เป็น Array ตัวอย่าง { "gameHistory": [3, 4, 7, 8, 5, 2, 1, 0], "pattern": 3 }
- ทำการนำประวัติการเล่น Array มา Mod 2 เพื่อแสดงผล XO
- เมื่อสุดสิ้นการเล่นจะทำการส่งประวัติการเล่นไปที่ BackEnd

**BackEnd : Logical Process** **: NodeJS, Express, File .Json, VS Code**

- ทำหน้าที่เก็บข้อมูลลงในไฟล์ data.json
- ทำหน้าที่จัดเก็บข้อมูลที่เข้ามาพร้อมกับเซ็ทค่าวันที่ ที่เข้ามาในช่วงเวลานั้นๆ (POST)
- ทำการส่งข้อมูลทั้งหมดที่มีใน data.json (GET)
- ทำการลบข้อมูลใน OBJ นั้นๆ โดยเปรียบเทียบกับประวัติการเล่นที่ส่งมาตรงกับส่วนไหนบ้าง

#

#

#

#

#

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/Digi-OHMER/game-tic-tac-toe-xo-4x4.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/Digi-OHMER/game-tic-tac-toe-xo-4x4/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

---

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thanks to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README

Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name

Choose a self-explaining name for your project.

## Description

Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges

On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals

Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation

Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage

Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support

Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap

If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing

State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment

Show your appreciation to those who have contributed to the project.

## License

For open source projects, say how it is licensed.

## Project status

If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
