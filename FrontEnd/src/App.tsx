import { useEffect, useState } from "react";
import cross_img from "./Assets/cross.png";
import circle_img from "./Assets/circle.png";
import { Modal, Table } from "antd";

interface ApiResponse {
  date: string;
  gameHistory: number[];
  pattern: number;
}

function App() {
  const [buttonXO, setButtonXO] = useState<Array<number>>([]);
  const [viewXO, setViewXO] = useState<Array<number>>([]);
  const [gameWin, setGameWin] = useState<boolean>(false);
  const [statusView, setStatusView] = useState<boolean>(false);
  const [winTitle, setWinTitle] = useState<string>("");
  const [showModal, setShowModal] = useState<boolean>(false);
  const [restAPI, setRestAPI] = useState<ApiResponse[]>([]);
  const [sizePatternINT, setSizePatternINT] = useState<number>(3);
  const [playWithAI, setPlayWithAI] = useState<boolean>(false);
  const [isPlayerTurn, setIsPlayerTurn] = useState<boolean>(true);
  const [stopDalay, setStopDalay] = useState<boolean>(false);

  const boxes =
    "flex h-[150px] w-[150px] bg-stone-600 border-8 border-[#0f1b21] rounded-3xl cursor-pointer flex justify-center items-center";
  const divReset = statusView ? "justify-between" : "justify-center";

  const onClickBox = (number: number) => {
    if (stopDalay) {
      return;
    }

    if (gameWin) {
      return;
    }

    const found = buttonXO.find((x) => x === number);
    if (found !== undefined) {
      return;
    }
    setButtonXO((prevButtonXO) => [...prevButtonXO, number]);

    if (playWithAI) {
      setIsPlayerTurn(!isPlayerTurn);
    }
  };

  const aiMove = () => {
    const value_x = buttonXO.filter((_item, i) => i % 2 === 0);
    const value_o = buttonXO.filter((_item, i) => i % 2 === 1);
    const allMoves = [...value_x, ...value_o];
    const availableMoves = Array.from(
      { length: sizePatternINT * sizePatternINT },
      (_, i) => i
    ).filter((i) => !allMoves.includes(i));

    const winPatterns = generateWinPatterns(sizePatternINT);

    const checkPotentialWin = (playerMoves: number[]) => {
      for (const pattern of winPatterns) {
        const moves = pattern.filter((index) => playerMoves.includes(index));
        if (moves.length === sizePatternINT - 1) {
          const move = pattern.find((index) => !allMoves.includes(index));
          if (move !== undefined) {
            return move;
          }
        }
      }
      return null;
    };

    // Check if AI can win
    let move = checkPotentialWin(value_o);
    if (move === null) {
      // Check if AI needs to block player
      move = checkPotentialWin(value_x);
    }
    if (move === null && availableMoves.length > 0) {
      // Pick a random available move if no winning or blocking move is found
      move = availableMoves[Math.floor(Math.random() * availableMoves.length)];
    }

    if (move !== null) {
      setButtonXO((prevButtonXO) => [...prevButtonXO, move]);
      setIsPlayerTurn(true);
    }
  };

  const generateWinPatterns = (sizePatterns: number) => {
    const winPatterns = [];
    // Rows
    for (let i = 0; i < sizePatterns; i++) {
      const row = [];
      for (let j = 0; j < sizePatterns; j++) {
        row.push(i * sizePatterns + j);
      }
      winPatterns.push(row);
    }
    // Columns
    for (let i = 0; i < sizePatterns; i++) {
      const col = [];
      for (let j = 0; j < sizePatterns; j++) {
        col.push(j * sizePatterns + i);
      }
      winPatterns.push(col);
    }
    // Diagonal 1
    const diag1 = [];
    for (let i = 0; i < sizePatterns; i++) {
      diag1.push(i * sizePatterns + i);
    }
    winPatterns.push(diag1);
    // Diagonal 2
    const diag2 = [];
    for (let i = 0; i < sizePatterns; i++) {
      diag2.push((i + 1) * sizePatterns - (i + 1));
    }
    winPatterns.push(diag2);

    return winPatterns;
  };

  useEffect(() => {
    let gameIsWin = false;
    const noWin = () => {
      if (buttonXO.length >= sizePatternINT * sizePatternINT) {
        setWinTitle("No one is win !!!");
        if (!statusView) {
          fetch("http://localhost:3000/api", {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              gameHistory: buttonXO,
              pattern: sizePatternINT,
            }),
          })
            .then((response) => {
              if (!response.ok) {
                throw new Error("Network response was not ok");
              }
              return response.text();
            })
            .then((data) => {
              console.log(data);
            })
            .catch((error) => {
              console.error("Error:", error);
            });
        }
      }
    };

    const updateXO = () => {
      const boxes = document.querySelectorAll(".box");
      buttonXO.forEach((x, i) => {
        const box = boxes[x];
        if (i % 2 === 0) {
          box.innerHTML = `<img class="h-[90px] w-[90px]" src="${cross_img}" alt="cross" />`;
        } else {
          box.innerHTML = `<img class="h-[90px] w-[90px]" src="${circle_img}" alt="circle" />`;
        }
      });
    };

    const checkWin = () => {
      const winPatterns = generateWinPatterns(sizePatternINT);
      const value_x = buttonXO.filter((_item, i) => i % 2 === 0);
      const value_o = buttonXO.filter((_item, i) => i % 2 === 1);

      const fetchPost = () => {
        if (!statusView) {
          fetch("http://localhost:3000/api", {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              gameHistory: buttonXO,
              pattern: sizePatternINT,
            }),
          })
            .then((response) => {
              if (!response.ok) {
                throw new Error("Network response was not ok");
              }
              return response.text();
            })
            .then((data) => {
              console.log(data);
            })
            .catch((error) => {
              console.error("Error:", error);
            });
        }
      };

      for (const pattern of winPatterns) {
        if (pattern.every((index) => value_x.includes(index))) {
          setGameWin(true);
          gameIsWin = true;
          setWinTitle("X is Winner !!!");
          fetchPost();
          return;
        }
        if (pattern.every((index) => value_o.includes(index))) {
          setGameWin(true);
          gameIsWin = true;
          setWinTitle("O is Winner !!!");
          fetchPost();
          return;
        }
      }
    };

    if (!statusView) {
      noWin();
    } else if (buttonXO.length >= sizePatternINT * sizePatternINT) {
      setWinTitle("No one is win !!!");
    }

    checkWin();
    updateXO();

    if (playWithAI && !isPlayerTurn && !gameIsWin) {
      setStopDalay(true);
      const aiMoveTimeout = setTimeout(() => {
        aiMove();
        setStopDalay(false);
      }, 200);

      return () => clearTimeout(aiMoveTimeout); // Clear timeout if effect runs again
    }
  }, [buttonXO, isPlayerTurn, playWithAI]);

  const onClickReset = () => {
    const boxes = document.querySelectorAll(".box");
    boxes.forEach((box) => {
      box.innerHTML = "";
    });

    setButtonXO([]);
    setViewXO([]);
    setGameWin(false);
    setStatusView(false);
    setWinTitle("");
    setShowModal(false);
    setRestAPI([]);
    setIsPlayerTurn(true);
  };

  const onClickView = () => {
    const boxes = document.querySelectorAll(".box");
    boxes.forEach((box) => {
      box.innerHTML = "";
    });

    setButtonXO([]);
    setViewXO([]);
    setGameWin(false);
    setStatusView(false);
    setWinTitle("");
    setShowModal(false);
  };

  const history = () => {
    fetch("http://localhost:3000/api")
      .then((response) => response.json())
      .then((data: ApiResponse[]) => {
        // onClickReset();
        setRestAPI(data);
        setShowModal(true);
      })
      .catch((error) => console.error("Error fetching data:", error));
  };

  const closeModal = () => {
    setShowModal(false);
  };

  const historyBack = () => {
    setButtonXO((prevButtonXO) => prevButtonXO.slice(0, -1));
    const boxes = document.querySelectorAll(".box");
    boxes.forEach((box) => {
      box.innerHTML = "";
    });
    setGameWin(true);
    setWinTitle("");
  };

  const historyNext = () => {
    if (buttonXO.length + 1 <= viewXO.length) {
      setButtonXO((prevButtonXO) => [...prevButtonXO, viewXO[buttonXO.length]]);
    }
  };

  const columns = [
    {
      title: "No.",
      dataIndex: "no",
      key: "number",
    },
    {
      title: "Patterns",
      dataIndex: "pattern",
      key: "pattern",
    },
    {
      title: "Date",
      dataIndex: "date",
      key: "date",
    },
    {
      title: "View",
      dataIndex: "view",
      key: "view",
    },
    {
      title: "Remove",
      dataIndex: "remove",
      key: "remove",
    },
  ];

  const setGameHistory = (history: number[], pattern: number) => {
    onClickView();

    setStatusView(true);
    setViewXO(history);
    setSizePatternINT(pattern);
    setGameWin(true);
    setButtonXO((prevButtonXO) => [...prevButtonXO, history[0]]);
  };

  const rmGameHistory = (history: number[]) => {
    setShowModal(false);
    fetch("http://localhost:3000/api", {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ history: history }),
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error("Network response was not ok");
        }
        return response.text();
      })
      .then((data) => {
        console.log(data);
      })
      .catch((error) => {
        console.error("Error:", error);
      });
  };

  const transformedData = restAPI.map((item, index) => ({
    no: index + 1,
    pattern: item.pattern,
    date:
      new Date(item.date).toLocaleString("th-TH", {
        timeZone: "Asia/Bangkok",
      }) + " น.",
    view: (
      <button
        className="flex h-[30px] w-[50px] bg-green-100 border border-[#0f1b21] rounded-lg flex justify-center items-center"
        onClick={() => {
          onClickReset();
          setGameHistory(item.gameHistory, item.pattern);
        }}
      >
        View
      </button>
    ),
    remove: (
      <button
        className="flex h-[30px] w-[70px] bg-red-300 border border-[#0f1b21] rounded-lg flex justify-center items-center"
        onClick={() => rmGameHistory(item.gameHistory)}
      >
        Remove
      </button>
    ),
  }));

  const loopDivRow = () => {
    const rows = [];
    for (let i = 0; i < sizePatternINT; i++) {
      const cols = [];
      for (let j = 0; j < sizePatternINT; j++) {
        const index = i * sizePatternINT + j;
        cols.push(
          <div
            key={index}
            className={`${boxes} box`}
            onClick={() => {
              onClickBox(index);
              // console.log("index: ", index);
            }}
          ></div>
        );
      }
      rows.push(
        <div key={i} className={`rows${i + 1} flex`}>
          {cols}
        </div>
      );
    }
    return rows;
  };

  return (
    <main className="text-center font-mono">
      <Modal
        title="Game History"
        open={showModal}
        onCancel={closeModal}
        footer=""
      >
        <Table columns={columns} dataSource={transformedData} />
      </Modal>
      <button
        type="button"
        className="h-[50px] w-[150px] bg-gray-800 rounded-xl text-xl text-yellow-200 absolute mr-10 right-0"
        onClick={() => history()}
      >
        history
      </button>
      <div className="absolute mr-10 mt-20 right-0">
        <label className="inline-flex items-center cursor-pointer">
          <span className="ms-3 mr-5 text-sm font-medium text-gray-900 dark:text-gray-300 text-xl">
            Play with Bot
          </span>
          <input
            type="checkbox"
            className="sr-only peer"
            checked={playWithAI}
            onChange={() => {
              setPlayWithAI(!playWithAI);
              if (!playWithAI) {
                onClickReset();
              }
            }}
          />
          <div className="relative w-11 h-6 bg-gray-200 peer-focus:outline-none peer-focus:ring-4 peer-focus:ring-blue-300 dark:peer-focus:ring-blue-800 rounded-full peer dark:bg-gray-700 peer-checked:after:translate-x-full rtl:peer-checked:after:-translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-[2px] after:start-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-5 after:w-5 after:transition-all dark:border-gray-600 peer-checked:bg-blue-600"></div>
        </label>
      </div>
      <h1 className="mt-6 text-white text-6xl flex justify-center items-center">
        GAME Tic Tac Toe - XO
        <span
          className="w-[50px] h-[55px] ml-8 cursor-pointer rounded-xl bg-red-400 flex justify-center items-center"
          onClick={() => {
            onClickReset();
            setSizePatternINT(
              sizePatternINT <= 3 ? sizePatternINT : sizePatternINT - 1
            );
          }}
        >
          -
        </span>
        ({`${sizePatternINT}*${sizePatternINT}`})
        <span
          className="w-[50px] h-[55px] cursor-pointer rounded-xl bg-green-400 flex justify-center items-center"
          onClick={() => {
            onClickReset();
            setSizePatternINT(sizePatternINT + 1);
          }}
        >
          +
        </span>
      </h1>

      <h1 className="m-7 text-green-600 font-bold text-5xl flex justify-center items-center">
        {winTitle}
      </h1>
      <div className="flex justify-center items-center">
        <div className="m-auto mt-6">{loopDivRow()}</div>
      </div>
      <div className="flex justify-center items-center">
        <div className={`rowButton w-[585px] flex ${divReset}`}>
          <button
            type="button"
            style={{ display: statusView ? "block" : "none" }}
            className="h-[80px] w-[100px] bg-gray-800 rounded-3xl text-3xl text-cyan-300 my-5"
            onClick={() => historyBack()}
          >
            {"<"}
          </button>
          <button
            type="button"
            className="h-[80px] w-[210px] bg-gray-800 rounded-3xl text-3xl text-cyan-300 my-5"
            onClick={() => onClickReset()}
          >
            reset
          </button>
          <button
            type="button"
            style={{ display: statusView ? "block" : "none" }}
            className="h-[80px] w-[100px] bg-gray-800 rounded-3xl text-3xl text-cyan-300 my-5"
            onClick={() => historyNext()}
          >
            {">"}
          </button>
        </div>
      </div>
    </main>
  );
}

export default App;
